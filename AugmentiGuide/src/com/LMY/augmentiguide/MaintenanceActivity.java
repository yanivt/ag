package com.LMY.augmentiguide;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.CountDownLatch;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class MaintenanceActivity extends Activity {
	final CountDownLatch ThreadHolder = new CountDownLatch(1); // will hold the main Thread until the second one finish
	int success=1;  // 0- failed , 1- success , 2-already updated
	private static File c; // will hold the Folder of the app on android
	String PathtoTmp;    // will hold the Path to the tmp folder
	String PathtoDB;    // will hold the path to the data base folder

	ConfigParser ftp;   // will hold all the ftp info (url , user , etc...)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);	
		MaintenanceActivity.c=getApplicationContext().getFilesDir(); // <----for the ftp


		setContentView(R.layout.maintenance_activity);


		Button start_Session = (Button)findViewById(R.id.startSession);
		start_Session.setOnClickListener(new View.OnClickListener() {

			@Override 
			public void onClick(View v) {

				Intent i = new Intent(getApplicationContext(),MainActivity.class);
				startActivity(i);
			} 
		}); 

		initVars();
		final TextView tvupdateinfo = (TextView) findViewById(R.id.textView1);
		final Button button = (Button) findViewById(R.id.button1);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				new Thread(new Runnable() {
					public void run() {	
						success=1;
						try {

							success=FTPDownloadFile.main(ftp , PathtoTmp, PathtoDB);
						} catch (IOException e) {
							e.printStackTrace();
							success=0;

						}

						if(success==1)
						{
							try {
								CopyFolder.CF(PathtoTmp,PathtoDB);
							} catch (IOException e) {
								e.printStackTrace();}}
						ThreadHolder.countDown();


					}
				}).start(); 
				try {
					ThreadHolder.await();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if(success==1){
					Date d=new Date();
					tvupdateinfo.setText("Successfully Updated at "+d.toString());
					
				}
				else if (success==2){
					tvupdateinfo.setText("Up To Date");
					
				}
				else if (success==0){
					tvupdateinfo.setText("Update Failed");
				}
			}
		});
	}
	private void initVars() { // will initalize the ftp from the 
							// xml , the Pathtosave-the temp folder , 
							// Desttosave - the real DB folder.
		
		
		
		final InputStream ftpxml=new BufferedInputStream(getResources().openRawResource(R.raw.config));
		PathtoTmp =new String( c.getPath()+"/tmp");
		PathtoDB = new String( c.getPath()+"/db");
		
		
		try {
			ftp= new ConfigParser(ftpxml);
		} catch (XmlPullParserException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}		
	}
	
}