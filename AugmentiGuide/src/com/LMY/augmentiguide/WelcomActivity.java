package com.LMY.augmentiguide;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
// 
//  TEST !!!!!!!
//
//
public class WelcomActivity extends Activity {
	 String sPass;
	 Button button_Password;
	 EditText text_Password;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);	
	    setContentView(R.layout.welcome_activity);	  
	    button_Password=(Button)findViewById(R.id.buttonPassword);
	    text_Password=(EditText)findViewById(R.id.textPassword);
	    
	    button_Password.setOnClickListener(new View.OnClickListener() {
	     
			    @Override 
			    public void onClick(View v) {
			    	
			    	sPass = text_Password.getText().toString();
			    	
			    	
			    	 if (sPass.equals(getPassword())) {
			    	        Intent i = new Intent(getApplicationContext(),MaintenanceActivity.class);
				            startActivity(i);
				            text_Password.setText("");
			          }else{
			        	    text_Password.setText("");  
			        	    alertWrongPass();
			          }
			    } 
	    }); 
	    
	   
	}
	
	
	private String getPassword() {
    	return "a";
    }
	private void alertWrongPass(){
		Toast.makeText(this, "Wrong Password",
        Toast.LENGTH_SHORT).show();
	}

	
}

////  test
