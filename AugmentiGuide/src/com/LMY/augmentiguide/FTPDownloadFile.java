package com.LMY.augmentiguide;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;


/*
 *		The class will Handle the FTP connection  
 *    	it will get the info from maintanceActivity check the 
 *    	dates on both the FTP and Android DB path 
 *    	if a download is needed , it will download to TMP folder 
 *      and call the CopyFolder To Handle TMP & DB folders
 */

public class FTPDownloadFile {
	// pathtarget will define the temp folder target on the android , currently  /data/data/com.LMY.augmentiguide/files/tmp 
	//  desttosave will define the folder of the real db  /data/data/com.LMY.augmentiguide/files/db
	public static int main( ConfigParser ftp ,String pathtarget, String desttosave) throws  IOException  {

		FTPClient ftpClient = new FTPClient();
		try {

			ftpClient.connect(ftp.getFtpServer(), ftp.getFtpPort());
			ftpClient.login(ftp.getFtpUser(), ftp.getFtpPass());
			ftpClient.enterLocalPassiveMode();
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			String parentDir=ftp.ftpParent;
			if (needsUpdate(ftpClient, new File(desttosave)))
				downloadDirectory(ftpClient,parentDir,"",pathtarget);
			else
				return 2;    

		} catch (IOException ex) {
			System.out.println("Error: " + ex.getMessage());
			ex.printStackTrace();
			return 0;
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
				return 0;
			}
		}
		return 1;
	}
	private static boolean needsUpdate(FTPClient ftpClient, File path) throws IOException { 
		// will true if an update is needed
		FTPFile ftpfile[] = ftpClient.listDirectories();
		System.out.println(path.getPath());
		long lm =0;
		for (int i=0;i<ftpfile.length;i++)
		{
			if(lm<ftpfile[i].getTimestamp().getTimeInMillis())
				lm=ftpfile[i].getTimestamp().getTimeInMillis();
		}
		if(path.lastModified()>=lm)// no need for update
		{
			System.out.println(" up to date  " +path.lastModified());
			return false;}
		System.out.println(" new update available "+lm);
		return true;
	}
	public static boolean downloadSingleFile(FTPClient ftpClient,String remoteFilePath, String savePath) throws IOException {
		// will download a single file
		File downloadFile = new File(savePath);

		File parentDir = downloadFile.getParentFile();
		if (!parentDir.exists()) {
			parentDir.mkdirs();
		}
		OutputStream outputStream = new BufferedOutputStream(
				new FileOutputStream(downloadFile));
		try {
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			return ftpClient.retrieveFile(remoteFilePath, outputStream);
		} catch (IOException ex) {
			throw ex;
		} finally {
			if (outputStream != null) {
				outputStream.close();
			}
		}
	}

	public static void downloadDirectory(FTPClient ftpClient, String parentDir, String currentDir, String saveDir) throws IOException {
		//will download a full directory recursively

		//   Note for config.xml -> Path needs to end in  '/'
		String dirToList = parentDir;
		if (!currentDir.equals("")) {
			dirToList += "/" + currentDir;
		}
		FTPFile[] subFiles = ftpClient.listFiles(dirToList);

		if (subFiles != null && subFiles.length > 0) {
			for (FTPFile aFile : subFiles) {
				String currentFileName = aFile.getName();
				if (currentFileName.equals(".") || currentFileName.equals("..")) {
					// skip parent directory and the directory itself
					continue;
				}
				String filePath = parentDir + "/" + currentDir + "/"
						+ currentFileName;
				if (currentDir.equals("")) {
					filePath = parentDir + "/" + currentFileName;
				}
				String newDirPath = saveDir + parentDir + File.separator
						+ currentDir + File.separator + currentFileName;
				if (currentDir.equals("")) {
					newDirPath = saveDir + parentDir + File.separator
							+ currentFileName;
				}
				if (aFile.isDirectory()) {
					// create the directory in saveDir
					File newDir = new File(newDirPath);
					boolean created = newDir.mkdirs();
					if (created) {
						System.out.println("CREATED the directory: " + newDirPath);
					} else {
						System.out.println("COULD NOT create the directory: " + newDirPath);
					}

					// download the sub directory
					downloadDirectory(ftpClient, dirToList, currentFileName,
							saveDir);
				} else {
					// download the file
					boolean success = downloadSingleFile(ftpClient, filePath,
							newDirPath);
					if (success) {
						System.out.println("DOWNLOADED the file: " + filePath);
					} else {
						System.out.println("COULD NOT download the file: "
								+ filePath);
					}
				}
			}
		}
	}
}