package com.LMY.augmentiguide;
import java.io.IOException;
import java.io.InputStream;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;


/*
 *  This class will return an Object containing 
 *  the FTP site info from the config.xml
 * 
 * 
 */


public class ConfigParser {
	String ftpServer;
	String ftpUser;
	String ftpPass;
	int ftpPort;
	String ftpParent;
	String ftpTarget;

	public ConfigParser(InputStream path) throws XmlPullParserException, IOException, java.text.ParseException
	{
		XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
		XmlPullParser myparser = xmlFactoryObject.newPullParser();


		myparser.setInput(path,null);
		int eventType = myparser.getEventType();
		while (eventType != XmlPullParser.END_DOCUMENT) {
			if(eventType == XmlPullParser.START_TAG && myparser.getName().equalsIgnoreCase("ftp-user")) {
				myparser.next();
				ftpUser = myparser.getText().replaceAll("\\s+","");;
			} else if(eventType == XmlPullParser.START_TAG && myparser.getName().equalsIgnoreCase("ftp-server")) {
				myparser.next();
				ftpServer = myparser.getText().replaceAll("\\s+","");;
			} else if(eventType == XmlPullParser.START_TAG && myparser.getName().equalsIgnoreCase("ftp-pass")) {
				myparser.next();
				ftpPass = myparser.getText().replaceAll("\\s+","");;
			} 
			else if(eventType == XmlPullParser.START_TAG && myparser.getName().equalsIgnoreCase("ftp-port")) {
				myparser.next();
				ftpPort= Integer.parseInt(myparser.getText().replaceAll("\\s+",""));
			} 
			else if(eventType == XmlPullParser.START_TAG && myparser.getName().equalsIgnoreCase("ftp-parent")) {
				myparser.next();
				ftpParent= myparser.getText().replaceAll("\\s+","");;
			} 
			eventType = myparser.next();
		}
		
	}



	public String getFtpServer() {
		return ftpServer;
	}
	public void setFtpServer(String ftpServer) {
		this.ftpServer = ftpServer;
	}
	public String getFtpUser() {
		return ftpUser;
	}
	public void setFtpUser(String ftpUser) {
		this.ftpUser = ftpUser;
	}
	public String getFtpPass() {
		return ftpPass;
	}
	public void setFtpPass(String ftpPass) {
		this.ftpPass = ftpPass;
	}
	public int getFtpPort() {
		return ftpPort;
	}
	public void setFtpPort(int ftpPort) {
		this.ftpPort= ftpPort;
	}
	

}
