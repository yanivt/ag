package com.LMY.augmentiguide;

import java.io.IOException;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity implements SurfaceHolder.Callback {
    
	Camera camera;
	SurfaceView surfaceView;
	SurfaceHolder surfaceHolder;
	boolean previewing = false;
	LayoutInflater controlInflater = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
	   
		
	    setContentView(R.layout.main_activity);
	    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);	
	    
	    getWindow().setFormat(PixelFormat.UNKNOWN);
	     surfaceView = (SurfaceView)findViewById(R.id.camerapreview);
	     surfaceHolder = surfaceView.getHolder();
	     surfaceHolder.addCallback(this);
	     surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	  
	     controlInflater = LayoutInflater.from(getBaseContext());
	     View viewControl = controlInflater.inflate(R.layout.camera_overlay, null);
	     LayoutParams layoutParamsControl
	      = new LayoutParams(LayoutParams.FILL_PARENT,
	      LayoutParams.FILL_PARENT);
	     this.addContentView(viewControl, layoutParamsControl);
	    
	}

	
	
		
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,int height) {
		// TODO Auto-generated method stub
	if(previewing){
		 camera.stopPreview();
		 previewing = false;
	}
	
	if (camera != null){
		 try {
			  camera.setPreviewDisplay(surfaceHolder);
			  camera.startPreview();
			  previewing = true;
		 } catch (IOException e) {
			  // TODO Auto-generated catch block
			  e.printStackTrace();
		 }
		}
	}
	
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		camera = Camera.open();
	}
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		camera.stopPreview();
		camera.release();
		camera = null;
		previewing = false;
	}
	//Disable the Back button
	@Override
	public void onBackPressed()
	{

	  
	}
}
