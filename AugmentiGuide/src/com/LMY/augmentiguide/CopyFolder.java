package com.LMY.augmentiguide;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
public class CopyFolder
{
/*
 *    The Class will handle the after download part. 
 *    Removing the DB folder (if exists)
 * 	  Copying the TMP folder to the DB folder
 * 	  And deleting the TMP folder
 */


	public static void CF(String src, String dest) throws IOException // MAIN
	{	
		File srcFolder = new File(src);
		File destFolder = new File(dest);
		if(destFolder.exists())
			delete(destFolder);
		//make sure source exists
		if(!srcFolder.exists()){

			System.out.println("Directory does not exist.");

		}else{

			try{
				copyFolder(srcFolder,destFolder);
				delete(srcFolder);
			}catch(IOException e){
				e.printStackTrace();
				//error, just exit
				System.exit(0);
			}
		}

		System.out.println("Done");
	}
	
	static void delete(File f) throws IOException { // will delete a directory completely
		if (f.isDirectory()) {
			for (File c : f.listFiles()){
				System.out.println(c.getName()+" was deleted");
				delete(c);}
		}
		if (!f.delete())
			throw new FileNotFoundException("Failed to delete file: " + f);
	}

	public static void copyFolder(File src, File dest) // will copy a full directory from one local path to another 
			throws IOException{

		if(src.isDirectory()){

			//if directory not exists, create it
			if(!dest.exists()){
				dest.mkdirs();
				System.out.println("Directory copied from " 
						+ src + "  to " + dest);
			}

			//list all the directory contents
			String files[] = src.list();

			for (String file : files) {
				//construct the src and dest file structure
				File srcFile = new File(src, file);
				File destFile = new File(dest, file);
				//recursive copy
				copyFolder(srcFile,destFile);
			}

		}else{
			//if file, then copy it
			//Use bytes stream to support all file types
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dest); 

			byte[] buffer = new byte[1024];

			int length;
			//copy the file content in bytes 
			while ((length = in.read(buffer)) > 0){
				out.write(buffer, 0, length);
			}

			in.close();
			out.close();
			System.out.println("File copied from " + src + " to " + dest);
		}
	}
}